curl -H "Content-Type: application/json" -XPOST "http://localhost:9200/netology/" -d "{ \"status_code\" : \"200\", \"timestamp\" : \"2021-12-08'T'15:19:00\"}"
curl -H "Content-Type: application/json" -XPOST "http://localhost:9200/netology/_doc" -d "{ \"status_code\" : \"200\", \"@timestamp" : \"2021-08-12T12:50:44.732Z\"}"


3662 - prometheus dashboard



Просмотр списка очередей

# rabbitmqctl list_queues
1
# rabbitmqctl list_queues
Просмотр списка очередей с выводом имен политик,которые применены к этим очередям

# rabbitmqctl list_queues name policy pid slave_pid
1
# rabbitmqctl list_queues name policy pid slave_pid
Ручная синхронизация очереди

# rabbitmqctl sync_queue <имя очереди>
1
# rabbitmqctl sync_queue <имя очереди>
Отмена синхронизации очереди

# rabbitmqctl cancel_sync_queue <имя очереди>
1
# rabbitmqctl cancel_sync_queue <имя очереди>
Проверка состояния RabbitMQ ноды

# rabbitmqctl node_health_check
1
# rabbitmqctl node_health_check
Timeout: 70.0 seconds
Checking health of node rabbit@app01
Health check passed
1
2
3
Timeout: 70.0 seconds
Checking health of node rabbit@app01
Health check passed
Просмотр статуса RabbitMQ-ноды

# rabbitmqctl status
1
# rabbitmqctl status
Полный отчет(включая состояние кластера, нод, политик, параметров, пользователей, вирт.хостов и т.д.)

# rabbitmqctl report | less
1
# rabbitmqctl report | less
Больше команд доступно по

# rabbitmqctl --help
1
# rabbitmqctl --help
