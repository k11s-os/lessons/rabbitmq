import time

import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='hello')
count = 0
while True:
    count += 1
    channel.basic_publish(exchange='', routing_key='hello', body='Hello Netology! Message:' + str(count))
    # time.sleep(60)

connection.close()
